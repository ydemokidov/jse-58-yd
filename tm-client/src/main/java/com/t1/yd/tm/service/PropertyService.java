package com.t1.yd.tm.service;

import com.t1.yd.tm.api.service.IPropertyService;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

@Service
@Getter
@Setter
@PropertySource("classpath:application.properties")
public class PropertyService implements IPropertyService {

    @Value("${server.host}")
    private String host;

    @Value("${server.port}")
    private String port;

    @Value("${version}")
    private String applicationVersion;

    @Value("${developer}")
    private String authorName;

    @Value("${email}")
    private String authorEmail;

    @Value("${password.iteration}")
    private String pwdIteration;

    @Value("${password.secret}")
    private String pwdSecret;

}
