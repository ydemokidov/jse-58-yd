package com.t1.yd.tm.component;

import com.t1.yd.tm.api.service.ILoggerService;
import com.t1.yd.tm.event.ConsoleEvent;
import com.t1.yd.tm.exception.system.ArgumentNotSupportedException;
import com.t1.yd.tm.exception.system.CommandNotSupportedException;
import com.t1.yd.tm.listener.AbstractListener;
import com.t1.yd.tm.util.SystemUtil;
import com.t1.yd.tm.util.TerminalUtil;
import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@Getter
@Component
public final class Bootstrap {

    @NotNull
    private final ILoggerService loggerService;

    @NotNull
    private final FileScanner fileScanner;

    @NotNull
    private final AbstractListener[] listeners;

    @NotNull
    private final ApplicationEventPublisher publisher;

    @Autowired
    public Bootstrap(@NotNull final ILoggerService loggerService,
                     @NotNull final FileScanner fileScanner,
                     @NotNull final AbstractListener[] listeners,
                     @NotNull final ApplicationEventPublisher publisher) {
        this.loggerService = loggerService;
        this.fileScanner = fileScanner;
        this.listeners = listeners;
        this.publisher = publisher;
    }

    public void run(@NotNull final String[] args) {
        processArguments(args);
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND:");
                @NotNull final String command = TerminalUtil.nextLine();
                publisher.publishEvent(new ConsoleEvent(command));
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (final Exception e) {
                e.getMessage();
                System.err.println("[FAIL]");
                loggerService.error(e);
            }
        }
    }

    @PostConstruct
    void prepareStartup() {
        initLogger();
        initPID();
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        final File file = new File(filename);
        file.deleteOnExit();
    }

    private void processArguments(@Nullable final String[] args) {
        if (args == null || args.length == 0) return;
        if (args[0] == null) return;
        processArgument(args[0]);
    }

    private void processArgument(@NotNull final String arg) {
        @Nullable final AbstractListener listener = getListenerByArgument(arg);
        if (listener == null) throw new ArgumentNotSupportedException();
        publisher.publishEvent(listener.getName());
    }

    public void processCommand(@NotNull final String commandName) {
        @Nullable final AbstractListener listener = getListenerByArgument(commandName);
        if (listener == null) throw new CommandNotSupportedException();
        publisher.publishEvent(listener.getName());
    }

    @Nullable
    private AbstractListener getListenerByArgument(@Nullable final String argument) {
        for (@Nullable final AbstractListener listener : listeners) {
            if (argument.equals(listener.getArgument())) return listener;
        }
        return null;
    }

    private void initLogger() {
        loggerService.info("** WELCOME TO TASK MANAGER **");
    }


    private void prepareShutdown() {
        fileScanner.stop();
        loggerService.info("** TASK MANAGER IS SHUTTING DOWN **");
    }

    private void exit() {
        System.exit(0);
    }


}