package com.t1.yd.tm.listener.data;

import com.t1.yd.tm.api.endpoint.IDataEndpoint;
import com.t1.yd.tm.api.service.ITokenService;
import com.t1.yd.tm.listener.AbstractListener;
import org.jetbrains.annotations.NotNull;

public abstract class AbstractDataListener extends AbstractListener {

    @NotNull
    private final IDataEndpoint dataEndpointClient;

    public AbstractDataListener(@NotNull final ITokenService tokenService,
                                @NotNull final IDataEndpoint dataEndpointClient) {
        super(tokenService);
        this.dataEndpointClient = dataEndpointClient;
    }

    @NotNull
    protected IDataEndpoint getDataEndpointClient() {
        return dataEndpointClient;
    }

}
