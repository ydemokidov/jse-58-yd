package com.t1.yd.tm.repository.dto;

import com.t1.yd.tm.api.repository.dto.IDtoSessionRepository;
import com.t1.yd.tm.dto.model.SessionDTO;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;

@Repository
@Scope("prototype")
public class SessionDtoRepository extends AbstractDtoUserOwnedRepository<SessionDTO> implements IDtoSessionRepository {

    @Autowired
    public SessionDtoRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    protected @NotNull String getEntityName() {
        return getClazz().getSimpleName();
    }

    @Override
    protected @NotNull Class<SessionDTO> getClazz() {
        return SessionDTO.class;
    }
}
