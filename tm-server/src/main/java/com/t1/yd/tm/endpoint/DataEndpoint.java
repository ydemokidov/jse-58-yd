package com.t1.yd.tm.endpoint;

import com.t1.yd.tm.api.endpoint.IDataEndpoint;
import com.t1.yd.tm.api.service.IAuthService;
import com.t1.yd.tm.api.service.IDomainService;
import com.t1.yd.tm.dto.request.data.*;
import com.t1.yd.tm.dto.response.data.*;
import com.t1.yd.tm.enumerated.Role;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Controller
@NoArgsConstructor
@WebService(endpointInterface = "com.t1.yd.tm.api.endpoint.IDataEndpoint")
public class DataEndpoint extends AbstractEndpoint implements IDataEndpoint {

    private IDomainService domainService;

    @Autowired
    public DataEndpoint(@NotNull final IAuthService authService,
                        @NotNull final IDomainService domainService) {
        super(authService);
        this.domainService = domainService;
    }

    @NotNull
    @Override
    @WebMethod
    public DataBackupLoadResponse backupLoad(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final DataBackupLoadRequest request) {
        check(request, Role.ADMIN);
        domainService.dataBackupLoad();
        return new DataBackupLoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataBackupSaveResponse backupSave(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final DataBackupSaveRequest request) {
        check(request, Role.ADMIN);
        domainService.dataBackupSave();
        return new DataBackupSaveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataBinaryLoadResponse binaryLoad(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final DataBinaryLoadRequest request) {
        check(request, Role.ADMIN);
        domainService.dataBinaryLoad();
        return new DataBinaryLoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataBinarySaveResponse binarySave(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final DataBinarySaveRequest request) {
        check(request, Role.ADMIN);
        domainService.dataBinarySave();
        return new DataBinarySaveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataBase64LoadResponse base64Load(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final DataBase64LoadRequest request) {
        check(request, Role.ADMIN);
        domainService.dataBase64Load();
        return new DataBase64LoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataBase64SaveResponse base64Save(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final DataBase64SaveRequest request) {
        check(request, Role.ADMIN);
        domainService.dataBase64Save();
        return new DataBase64SaveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataFasterXmlJsonSaveResponse fasterXmlJsonSave(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final DataFasterXmlJsonSaveRequest request) {
        check(request, Role.ADMIN);
        domainService.dataFasterXmlJsonSave();
        return new DataFasterXmlJsonSaveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataFasterXmlJsonLoadResponse fasterXmlJsonLoad(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final DataFasterXmlJsonLoadRequest request) {
        check(request, Role.ADMIN);
        domainService.dataFasterXmlJsonLoad();
        return new DataFasterXmlJsonLoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataFasterXmlXmlSaveResponse fasterXmlXmlSave(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final DataFasterXmlXmlSaveRequest request) {
        check(request, Role.ADMIN);
        domainService.dataFasterXmlXmlSave();
        return new DataFasterXmlXmlSaveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataFasterXmlXmlLoadResponse fasterXmlXmlLoad(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final DataFasterXmlXmlLoadRequest request) {
        check(request, Role.ADMIN);
        domainService.dataFasterXmlXmlLoad();
        return new DataFasterXmlXmlLoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataFasterXmlYmlSaveResponse fasterXmlYmlSave(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final DataFasterXmlYmlSaveRequest request) {
        check(request, Role.ADMIN);
        domainService.dataFasterXmlYmlSave();
        return new DataFasterXmlYmlSaveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataFasterXmlYmlLoadResponse fasterXmlYmlLoad(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final DataFasterXmlYmlLoadRequest request) {
        check(request, Role.ADMIN);
        domainService.dataFasterXmlYmlLoad();
        return new DataFasterXmlYmlLoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataJaxbJsonLoadResponse jaxbJsonLoad(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final DataJaxbJsonLoadRequest request) {
        check(request, Role.ADMIN);
        domainService.dataJaxbJsonLoad();
        return new DataJaxbJsonLoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataJaxbJsonSaveResponse jaxbJsonSave(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final DataJaxbJsonSaveRequest request) {
        check(request, Role.ADMIN);
        domainService.dataJaxbJsonSave();
        return new DataJaxbJsonSaveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataJaxbXmlLoadResponse jaxbXmlLoad(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final DataJaxbXmlLoadRequest request) {
        check(request, Role.ADMIN);
        domainService.dataJaxbXmlLoad();
        return new DataJaxbXmlLoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataJaxbXmlSaveResponse jaxbXmlSave(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final DataJaxbXmlSaveRequest request) {
        check(request, Role.ADMIN);
        domainService.dataJaxbXmlSave();
        return new DataJaxbXmlSaveResponse();
    }

}
