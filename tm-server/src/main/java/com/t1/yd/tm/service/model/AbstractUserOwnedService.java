package com.t1.yd.tm.service.model;

import com.t1.yd.tm.api.repository.model.IUserOwnedRepository;
import com.t1.yd.tm.api.service.ILoggerService;
import com.t1.yd.tm.api.service.model.IUserOwnedService;
import com.t1.yd.tm.enumerated.Sort;
import com.t1.yd.tm.exception.entity.EntityNotFoundException;
import com.t1.yd.tm.exception.field.IdEmptyException;
import com.t1.yd.tm.exception.field.IndexIncorrectException;
import com.t1.yd.tm.exception.field.UserIdEmptyException;
import com.t1.yd.tm.model.AbstractUserOwnedEntity;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.ApplicationContext;

import javax.persistence.EntityManager;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnedService<E extends AbstractUserOwnedEntity, R extends IUserOwnedRepository<E>> extends AbstractService<E, R> implements IUserOwnedService<E> {

    public AbstractUserOwnedService(@NotNull final ILoggerService loggerService,
                                    @NotNull final ApplicationContext context) {
        super(loggerService, context);
    }

    @Override
    public E add(@NotNull final String userId, @NotNull final E entity) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final R repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.add(userId, entity);
            entityManager.getTransaction().commit();
            return entity;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void clear(@NotNull final String userId) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final R repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.clear(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public List<E> findAll(@NotNull final String userId) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final R repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findAll(userId);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public List<E> findAll(@NotNull final String userId, @Nullable final Comparator comparator) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final R repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            if (comparator == null) return findAll(userId);
            return repository.findAll(userId, comparator);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    public List<E> findAll(@NotNull final String userId, @Nullable final Sort sort) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (sort == null) return findAll(userId);
        return findAll(userId, sort.getComparator());
    }

    @Nullable
    @Override
    public E findOneById(@NotNull final String userId, @NotNull final String id) {
        if (id.isEmpty()) throw new IdEmptyException();
        if (userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final R repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findOneById(userId, id);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public E findOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        if (index < 0) throw new IndexIncorrectException();
        if (userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final R repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findOneByIndex(userId, index);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public E removeById(@NotNull final String userId, @NotNull final String id) {
        if (id.isEmpty()) throw new IdEmptyException();
        if (userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final R repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @Nullable final E result = findOneById(userId, id);
            if (result == null) throw new EntityNotFoundException();
            repository.removeById(userId, id);
            entityManager.getTransaction().commit();
            return result;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public E removeByIndex(@NotNull final String userId, @NotNull final Integer index) {
        if (index < 0) throw new IndexIncorrectException();
        if (userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final R repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @Nullable final E result = findOneByIndex(userId, index);
            if (result == null) throw new EntityNotFoundException();
            repository.removeByIndex(userId, index);
            entityManager.getTransaction().commit();
            return result;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final R repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            if (id.isEmpty()) throw new IdEmptyException();
            return repository.findOneById(userId, id) != null;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public int getSize(@NotNull final String userId) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final R repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findAll(userId).size();
        } finally {
            entityManager.close();
        }
    }

}
