package com.t1.yd.tm.service.model;

import com.t1.yd.tm.api.repository.model.ISessionRepository;
import com.t1.yd.tm.api.service.ILoggerService;
import com.t1.yd.tm.api.service.model.ISessionService;
import com.t1.yd.tm.model.Session;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

@Service
public class SessionService extends AbstractUserOwnedService<Session, ISessionRepository> implements ISessionService {

    @Autowired
    public SessionService(@NotNull final ILoggerService loggerService,
                          @NotNull final ApplicationContext context) {
        super(loggerService, context);
    }

    @NotNull
    @Override
    protected ISessionRepository getRepository() {
        return context.getBean(ISessionRepository.class);
    }

}
