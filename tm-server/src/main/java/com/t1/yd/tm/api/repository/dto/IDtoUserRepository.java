package com.t1.yd.tm.api.repository.dto;

import com.t1.yd.tm.dto.model.UserDTO;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IDtoUserRepository extends IDtoRepository<UserDTO> {

    @Nullable UserDTO findByLogin(@NotNull String login);

    @Nullable UserDTO findByEmail(@NotNull String email);

}
