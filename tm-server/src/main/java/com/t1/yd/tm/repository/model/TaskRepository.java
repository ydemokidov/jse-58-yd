package com.t1.yd.tm.repository.model;

import com.t1.yd.tm.api.repository.model.ITaskRepository;
import com.t1.yd.tm.model.Task;
import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
@Scope("prototype")
public class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @Autowired
    public TaskRepository(EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public @NotNull List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        @NotNull final String query = String.format("FROM %s WHERE project.id = :projectId", getEntityName());
        return entityManager.createQuery(query, getClazz()).setParameter("projectId", projectId).setHint(QueryHints.HINT_CACHEABLE, true).getResultList();
    }

    @Override
    protected @NotNull String getEntityName() {
        return getClazz().getSimpleName();
    }

    @Override
    protected @NotNull Class<Task> getClazz() {
        return Task.class;
    }

}
