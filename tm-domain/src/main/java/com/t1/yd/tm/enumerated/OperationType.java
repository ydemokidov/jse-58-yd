package com.t1.yd.tm.enumerated;

public enum OperationType {

    INSERT,
    UPDATE,
    DELETE

}
